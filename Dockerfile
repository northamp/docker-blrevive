FROM alpine:3.17.3

# Copy resources then create the user - done first for caching purposes
COPY ./wine /builder/wine
COPY ./redist /opt/blacklightre/redist

RUN echo "x86" > /etc/apk/arch && apk add --no-cache --allow-untrusted /builder/wine/blrevive-server-wine-[0-9]*-r*.apk && \
    apk add --no-cache gnutls tzdata ca-certificates xvfb && \
    rm -rf /builder

RUN adduser -Dh /blrevive blrevive

USER blrevive

ENV WINEDEBUG=-d3d
ENV WINEPREFIX="/blrevive/.wine"
RUN WINEDLLOVERRIDES="mscoree,mshtml=,winemenubuilder.exe" wineboot --init && \
    for x in \
        /home/blrevive/.wine/drive_c/"Program Files"/"Common Files"/System/*/* \
        /home/blrevive/.wine/drive_c/windows/* \
        /home/blrevive/.wine/drive_c/windows/system32/* \
        /home/blrevive/.wine/drive_c/windows/system32/drivers/* \
        /home/blrevive/.wine/drive_c/windows/system32/wbem/* \
        /home/blrevive/.wine/drive_c/windows/system32/spool/drivers/x64/*/* \
        /home/blrevive/.wine/drive_c/windows/system32/Speech/common/* \
        /home/blrevive/.wine/drive_c/windows/winsxs/*/* \
    ; do \
        orig="/usr/lib/wine/i386-windows/$(basename "$x")"; \
        if cmp -s "$orig" "$x"; then ln -sf "$orig" "$x"; fi; \
    done && \
    for x in \
        /home/blrevive/.wine/drive_c/windows/globalization/sorting/*.nls \
        /home/blrevive/.wine/drive_c/windows/system32/*.nls \
    ; do \
        orig="/usr/share/wine/nls/$(basename "$x")"; \
        if cmp -s "$orig" "$x"; then ln -sf "$orig" "$x"; fi; \
    done && \
    # Load BLRE's DINPUT8.dll - Doesn't work for some reason
    wine reg add 'HKEY_CURRENT_USER\Software\Wine\DllOverrides' /v dinput8 /t REG_SZ /d native

USER root

# Artemis is bound to be the thing that changes the most, handling it as late as possible
COPY ./artemis/artemis-linux-amd64 /usr/local/bin/artemis

RUN chmod +x /usr/local/bin/artemis && \
  # Prepare directories for Artemis - owner is blrevive, but unless artemis' auto update is enabled mount-points can be readonly (that's the entire purpose of the symlinks after all!)
  install -d -m 0755 -o blrevive /mnt/blacklightre/game /mnt/blacklightre/blre /mnt/blacklightre/modules /mnt/blacklightre/config && \
  # silence Xvfb xkbcomp warnings by working around the bug (present in libX11 1.7.2) fixed in libX11 1.8 by https://gitlab.freedesktop.org/xorg/lib/libx11/-/merge_requests/79
  echo 'partial xkb_symbols "evdev" {};' > /usr/share/X11/xkb/symbols/inet

USER blrevive

VOLUME /mnt/blacklightre/game
VOLUME /mnt/blacklightre/blre
VOLUME /mnt/blacklightre/modules
VOLUME /mnt/blacklightre/config

ENV WINEDLLOVERRIDES="explorer.exe=d;services.exe=d;plugplay.exe=d;svchost.exe=d;rpcss.exe=d;dinput8=n"
ENV WINEDEBUG="-all"

ENV BLREVIVE_DIRS_BLR="/mnt/blacklightre/game"
ENV BLREVIVE_DIRS_BLRE="/mnt/blacklightre/blre"
ENV BLREVIVE_DIRS_BLRECONFIG="/mnt/blacklightre/config"
ENV BLREVIVE_DIRS_MODULES="/mnt/blacklightre/modules"
# Baked into the image, can be overriden if need be
ENV BLREVIVE_DIRS_REDIST="/opt/blacklightre/redist"

EXPOSE 7777/udp
EXPOSE 7777/tcp

ENTRYPOINT ["artemis"]
CMD ["start"]