# BLRevive Docker Server

<!-- markdownlint-disable-next-line MD033 MD045-->
<img title="M.A.R.S. doodle. These Wine builds take ages, man" align="right" height="275" width="330" src="https://gitlab.com/northamp/docker-blrevive/-/raw/master/mars.png" >

A Docker implementation of the [Blacklight: Retribution Revive](https://gitlab.com/blrevive) server.

Relies on [Artemis](https://gitlab.com/northamp/artemis-blrevive). Details about its usage are in its documentation, and a full setup procedure can be found on [BLRevive's Wiki](https://blrevive.gitlab.io/wiki/).

**NOTE**: Requires a dual-core processor due to a BL:R warning that cannot be acknowledged on headless instances (yet). Use at your own risks!

## Usage

See the [Wiki](https://blrevive.gitlab.io/wiki/) for an exhaustive setup procedure.

### Volumes

Two volumes are mandatory:

* `/mnt/blacklightre/game`: Contains the game itself
* `/mnt/blacklightre/config`: BLRevive configurations you'd like to use

And optionally two other volumes can be set up for BLRevive resources:

* `/mnt/blacklightre/blre`: BLRevive's DLLs
* `/mnt/blacklightre/modules`: Modules you'd like to use

Artemis is able to download the bare essentials (BLRE and server-utils), and will store them at those locations by default.

Assuming the data located on the host's `/srv/blacklightre`, this would result in (`ro` flag for volumes is optional and not compatible with Artemis' auto update feature, but recommended otherwise):

```bash
docker run --rm --env BLREVIVE_GAMESETTINGS_NUMBOTS=2 \
  -v /srv/blacklightre/game:/mnt/blacklightre/game:ro \
  -v /srv/blacklightre/blre:/mnt/blacklightre/blre:ro \
  -v /srv/blacklightre/modules:/mnt/blacklightre/modules:ro \
  -v /srv/blacklightre/config:/mnt/blacklightre/config \
  -p 7777:7777/udp -p 7777:7777/tcp \
  registry.gitlab.com/northamp/docker-blrevive:latest
```

### Configuration

The image's entrypoint (Artemis) expects several environment variables, none of whom are mandatory (due to the essentials being preconfigured in the image).

You can find the exhaustive list in Artemis' README or the Wiki, but here are some of the more relevant ones:

| Name                               | Description                                                                   |
| ---------------------------------- | ----------------------------------------------------------------------------- |
| `BLREVIVE_DEBUG`                   | If set to `true`, enables debug mode for more verbose logging, among others   |
| `BLREVIVE_LOG_JSON`                | If set to `true`, enables JSON-formatted logs                                 |
| `BLREVIVE_GAMESETTINGS_CONFIGNAME` | Name of the configuration BLRevive should look for in the `config` dir        |
| `BLREVIVE_GAMESETTINGS_SERVERNAME` | Advertised server name                                                        |
| `BLREVIVE_GAMESETTINGS_PASSWORD`   | Password players will need to enter to join                                   |
| `BLREVIVE_GAMESETTINGS_PLAYLIST`   | Playlist that should be used. *Can be a custom one created with server-utils* |
| `BLREVIVE_GAMESETTINGS_MAP`        | If no playlist is set, server will use that map                               |
| `BLREVIVE_GAMESETTINGS_GAMEMODE`   | If no playlist is set, server will use that gamemode                          |
| `BLREVIVE_GAMESETTINGS_MAXPLAYERS` | Number of players allowed in the server                                       |
| `BLREVIVE_GAMESETTINGS_NUMBOTS`    | Number of bots. *Might be overriden by the playlist*                          |
| `BLREVIVE_GAMESETTINGS_TIMELIMIT`  | Round time limit                                                              |
| `BLREVIVE_GAMESETTINGS_SCP`        | Amount of SCP players start with                                              |
| `BLREVIVE_GAMESETTINGS_CUSTOM`     | Additional launch args to pass to the server (`?param=value?param2=value`)    |

## Credits/Acknowledgements

### Wine build

Though the CI and imaging processes are now different, they are essentially based on [pg9182/northstar-dedicated](https://github.com/pg9182/northstar-dedicated/)'s excellent zlib/libpng licensed project.

### MagicCow

Previous versions up to the Golang entrypoint rewrite were similar to [MagicCow's MIT licensed Discord bot](https://github.com/MajiKau/BLRE-Server-Info-Discord-Bot).
